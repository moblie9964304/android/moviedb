package com.example.moviejava.ui.film;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.moviejava.R;
import com.example.moviejava.databinding.ActivityFilmBinding;
import com.example.moviejava.model.film.Film;
import com.example.moviejava.ui.film.adapter.CastAdapter;
import com.example.moviejava.ui.film.adapter.CompaniesAdapter;
import com.example.moviejava.ui.film.adapter.CountriesAdapter;
import com.example.moviejava.ui.film.adapter.CrewAdapter;
import com.example.moviejava.ui.home.fragment.genres.GenresAdapter;
import com.example.moviejava.ui.person.PersonActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;


@AndroidEntryPoint
public class FilmActivity extends AppCompatActivity {
    private ActivityFilmBinding binding;
    private FilmViewModel viewModel;
    @Inject
    CompaniesAdapter companiesAdapter;
    @Inject
    CountriesAdapter countriesAdapter;
    @Inject
    CastAdapter castAdapter;
    @Inject
    CrewAdapter crewAdapter;
    @Inject
    GenresAdapter genresAdapter;
    private Film f;
   List< String> key = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityFilmBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        onSetActionBar();
        viewModel = new ViewModelProvider(this).get(FilmViewModel.class);
        Bundle bundle = getIntent().getExtras();
        viewModel.loadData(bundle.getInt("film_id"));
        dataOnChange();
        setView();
        setEvenAdapter();
        onShowText();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            case R.id.add_wish_list: {
                viewModel.addToDb(f);
                Toast.makeText(this, "Added to favorite", Toast.LENGTH_SHORT).show();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void onSetActionBar() {
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back_24);
    }

    //set show more or show less text view
    private void onShowText() {
        binding.overviewFilm.setShowingLine(4);
        binding.overviewFilm.setShowLessTextColor(Color.parseColor("#99FFFF"));
        binding.overviewFilm.setShowMoreTextColor(Color.parseColor("#99FFFF"));
        binding.overviewFilm.addShowLessText("hide");
        binding.overviewFilm.addShowMoreText("read more");

    }

    private void dataOnChange() {
        viewModel.film.observe(this, it -> {
            f = it;
            binding.setF(it);
            binding.executePendingBindings();
            companiesAdapter.submitData(it.productionCompanies);
            countriesAdapter.submitData(it.productionCountries);
            genresAdapter.submitData(it.genres);
        });
        viewModel.cast.observe(this, it -> {
            castAdapter.submitData(it.cast);
            crewAdapter.submitData(it.crew);
        });
        viewModel.video.observe(this, it ->
        {
            if (it.results.size() > 0)
                for (int i = 0; i < it.results.size(); i++) {
                    key.add(it.results.get(i).key);
                }
        });
    }

    private void setView() {
        binding.rcvGenres.setAdapter(genresAdapter);
        binding.rcvGenres.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        binding.rcvCast.setAdapter(castAdapter);
        binding.rcvCast.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        binding.rcvCrew.setAdapter(crewAdapter);
        binding.rcvCrew.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        binding.rcvCompanies.setAdapter(companiesAdapter);
        binding.rcvCompanies.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        binding.rcvCountries.setAdapter(countriesAdapter);
        binding.rcvCountries.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    private void setEvenAdapter() {
        castAdapter.onClick = cast -> {
            Intent intent = new Intent(this, PersonActivity.class);
            intent.putExtra("person_id", cast.id);
            startActivity(intent);
        };
        crewAdapter.onClick = crew -> {
            Intent intent = new Intent(this, PersonActivity.class);
            intent.putExtra("person_id", crew.id);
            startActivity(intent);
        };
        genresAdapter.onClickItem = genres -> {

        };
        binding.viewTrailer.setOnClickListener(
                view -> {
                    Intent intent = new Intent(this, YoutubeActivity.class);
                    intent.putExtra("video_key", key.toArray(new String [0]));
                    startActivity(intent);
                }
        );
    }

}