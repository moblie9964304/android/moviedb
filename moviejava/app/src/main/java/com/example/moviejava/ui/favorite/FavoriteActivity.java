package com.example.moviejava.ui.favorite;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.moviejava.R;
import com.example.moviejava.databinding.ActivityFavoriteBinding;
import com.example.moviejava.ui.film.FilmActivity;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;


@AndroidEntryPoint
public class FavoriteActivity extends AppCompatActivity {
    private ActivityFavoriteBinding binding;
    private FavoriteViewModel viewModel;
    @Inject
    FavoriteAdapter favoriteAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        binding = ActivityFavoriteBinding.inflate(getLayoutInflater());
        super.onCreate(savedInstanceState);
        setContentView(binding.getRoot());
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back_24);
        viewModel = new ViewModelProvider(this).get(FavoriteViewModel.class);
        binding.rcvFavorite.setAdapter(favoriteAdapter);
        binding.rcvFavorite.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        setEventListener();
        onDataChange();


    }

    private void setEventListener() {
        favoriteAdapter.onClick = movie -> {
            Intent intent = new Intent(this, FilmActivity.class);
            intent.putExtra("film_id", movie.id);
            startActivity(intent);
        };
        favoriteAdapter.onDelete = m -> viewModel.deleteRecord(m);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("SetTextI18n")
    private void onDataChange() {
        viewModel.movies.observe(this, it -> {

            if (it.size() == 0) {
                binding.progressBar.setVisibility(View.GONE);
                binding.textNotification.setText("There are no items in favorites");
            } else {
                favoriteAdapter.submitData(it);
                binding.progressBar.setVisibility(View.GONE);
                binding.textNotification.setVisibility(View.GONE);
            }
        });
    }
}