package com.example.moviejava.data.remote.film;


import com.example.moviejava.model.casts.CastsFilm;
import com.example.moviejava.model.film.Film;
import com.example.moviejava.model.film.video.Video;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.Retrofit;

@Singleton
public class FilmRepository {
    @Inject
    public FilmRepository(Retrofit retrofit) {
        this.rf = retrofit.create(FilmService.class);
    }

    private final FilmService rf;

    public Observable<Film> getFilm(int id) {
        return rf.getFilm(id).subscribeOn(Schedulers.io());
    }
    public  Observable<Video> getVideo(int id)
    {
        return  rf.getVideo(id).subscribeOn(Schedulers.io());
    }
    public  Observable<CastsFilm> getCast(int id)
    {
        return  rf.getCastFilm(id).subscribeOn(Schedulers.io());
    }
}