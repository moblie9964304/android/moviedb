package com.example.moviejava.ui.film;

import android.os.Bundle;
import android.util.Log;

import com.example.moviejava.R;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import java.util.Arrays;
import java.util.List;

public class YoutubeActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {
    private static final String key = "AIzaSyAUkXpB5cX0hK6Hmc3Dl_aSZBCpSLlPACc";
    private String[] k;
    private int position = 0;
    private YouTubePlayerView youTubePlayerView;
    List<String> list;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube);
        youTubePlayerView = findViewById(R.id.youtube_player);
        Bundle bundle = getIntent().getExtras();
        k = bundle.getStringArray("video_key");
        list= Arrays.asList(k);
        youTubePlayerView.initialize(key, this);

    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        if (k != null) {
            youTubePlayer.cueVideos(list);
            youTubePlayer.setPlaylistEventListener(new YouTubePlayer.PlaylistEventListener() {
                @Override
                public void onPrevious() {
                    if (position == 0)
                        position = k.length - 1;

                    else position--;
                    youTubePlayerView.initialize(key, YoutubeActivity.this);
                }

                @Override
                public void onNext() {
                    if (position == k.length)
                        position = 0;
                    else position++;
                    youTubePlayerView.initialize(key, YoutubeActivity.this);
                }

                @Override
                public void onPlaylistEnded() {
                    if (position != k.length - 1) {
                        position++;
                        youTubePlayerView.initialize(key, YoutubeActivity.this);
                    }
                }
            });
            youTubePlayer.play();
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Log.e("InitializationResult", youTubeInitializationResult.toString());
    }


}