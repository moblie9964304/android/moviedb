package com.example.caculatorapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.caculatorapp.databinding.ActivityMainBinding;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;
    private String textInput = "";
    private double result = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        handleEvent();
    }

    private void inputCalculation(String input) {

        if (textInput.length() == 0) {
            if (input.matches("[-0-9]"))
                textInput += input;
            if (input.matches("[.]"))
                textInput += "0" + input;
        } else {
            String lastCharacter = textInput.substring(textInput.length() - 1);
            if (input.matches("[0-9]") && !lastCharacter.equals("%"))
                textInput += input;
            if (input.matches("[-+/*%]")) {
                if (lastCharacter.matches("[-+*/.]")) {
                    textInput = textInput.substring(0, textInput.length() - 1);
                }
                textInput += input;
            }

            if (input.matches("[.]")) {
                int positionComma = -1;
                int index = 0;
                if (lastCharacter.matches("[-+*/]")) {
                    textInput += "0" + input;
                } else {
                    if (!lastCharacter.equals("%")) {
                        for (int i = textInput.length() - 1; i >= 0; i--) {
                            if (String.valueOf(textInput.charAt(i)).matches("[.]"))
                                positionComma = i;
                            if (String.valueOf(textInput.charAt(i)).matches("[-+*/]") && textInput.length() > 1) {
                                index = i;
                                break;
                            }
                        }
                        if (positionComma == -1 && index != 0) {
                            double d = Double.parseDouble(textInput.substring(index));
                            if (d == ((double) (int) d))
                                textInput += input;

                        }
                    }

                }
            }
        }
    }

    private void setTextInput() {
        binding.textInput.setText(textInput);
    }

    private void handleEvent() {
        binding.buttonClear.setOnClickListener(v -> {
            textInput = "";
            setTextInput();
        });
        binding.buttonDelete.setOnClickListener(v -> {
            if (textInput.length() > 0) {
                textInput = textInput.substring(0, textInput.length() - 1);
                setTextInput();
            }
        });
        binding.buttonPercent.setOnClickListener(v -> {
            inputCalculation("%");
            setTextInput();
        });
        binding.buttonDivision.setOnClickListener(v -> {
            inputCalculation("/");
            setTextInput();
        });
        binding.buttonMultiple.setOnClickListener(v -> {
            inputCalculation("*");
            setTextInput();
        });
        binding.buttonNine.setOnClickListener(v -> {
            inputCalculation("9");
            setTextInput();
            calculate();
        });
        binding.buttonEight.setOnClickListener(v -> {
            inputCalculation("8");
            setTextInput();
            calculate();
        });
        binding.buttonSeven.setOnClickListener(v -> {
            inputCalculation("7");
            setTextInput();
            calculate();
        });
        binding.buttonMinus.setOnClickListener(v -> {
            inputCalculation("-");
            setTextInput();
        });
        binding.buttonSix.setOnClickListener(v -> {
            inputCalculation("6");
            setTextInput();
            calculate();
        });
        binding.buttonFive.setOnClickListener(v -> {
            inputCalculation("5");
            setTextInput();
            calculate();
        });
        binding.buttonFour.setOnClickListener(v -> {
            inputCalculation("4");
            setTextInput();
            calculate();
        });
        binding.buttonPlus.setOnClickListener(v -> {
            inputCalculation("+");
            setTextInput();
        });
        binding.buttonThree.setOnClickListener(v -> {
            inputCalculation("3");
            setTextInput();
            calculate();
        });
        binding.buttonTwo.setOnClickListener(v -> {
            inputCalculation("2");
            setTextInput();
            calculate();
        });
        binding.buttonOne.setOnClickListener(v -> {
            inputCalculation("1");
            setTextInput();
            calculate();
        });
        binding.buttonZero.setOnClickListener(v -> {
            inputCalculation("0");
            setTextInput();
            calculate();
        });
        binding.buttonComma.setOnClickListener(v -> {
            inputCalculation(".");
            setTextInput();
        });
        binding.buttonEqual.setOnClickListener(v -> {
            calculate();
        });
    }

    private void calculate() {
        String textResult = "=";
        if (textInput.substring(textInput.length() - 1).matches("[-+*/.]"))
            textInput = textInput.substring(0, textInput.length() - 1);

        List<String> numbers = new ArrayList<>(Arrays.asList(textInput.split("[-+*/]")));
        List<String> calculates = new ArrayList<>(Arrays.asList(textInput.split("[%]|\\d+")));
        if (numbers.get(0).equals("")) {
            numbers.set(1, "-" + numbers.get(1));
            numbers.remove(0);
            calculates.remove(0);
        }
        for (int i = 0; i < calculates.size(); i++) {
            if (calculates.get(i).equals(".") || calculates.get(i).equals("")||calculates.get(i).equals("%")) {
                calculates.remove(i);
                i--;
            }
        }
        for (int i = 0; i < numbers.size(); i++) {
            if (Pattern.compile("[%]").matcher(numbers.get(i)).find())
                numbers.set(i, String.valueOf((Double.parseDouble(numbers.get(i).substring(0, numbers.get(i).length() - 1))) / 100));
        }
        for (int i = 0; i < calculates.size(); i++) {
            if (calculates.get(i).equals("/")) {
                numbers.set(i, String.valueOf(Double.parseDouble(numbers.get(i)) / Double.parseDouble(numbers.get(i + 1))));
                numbers.remove(i + 1);
                calculates.remove(i);
                i--;
            }
            if (calculates.get(i).equals("*")) {
                numbers.set(i, String.valueOf(Double.parseDouble(numbers.get(i)) * Double.parseDouble(numbers.get(i + 1))));
                numbers.remove(i + 1);
                calculates.remove(i);
                i--;
            }
        }
        result = Double.parseDouble(numbers.get(0));
        for (int i = 0; i < calculates.size(); i++) {
            if (calculates.get(i).equals("-"))
                result -= Double.parseDouble(numbers.get(i + 1));
            else result += Double.parseDouble(numbers.get(i + 1));
        }
        if (result == ((double) (int) result)) {
            textResult += (int) result;
        } else textResult += result;
        binding.textResult.setText(textResult);

    }
}